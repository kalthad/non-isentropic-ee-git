\select@language {english}
\contentsline {section}{\tocsection {}{1}{Introduction}}{1}{section.1}
\contentsline {section}{\tocsection {}{2}{The relativistic Euler equations with Entropy}}{1}{section.2}
\contentsline {section}{\tocsection {}{3}{An Example of an equation of state for the non--isentropic Euler equations}}{3}{section.3}
\contentsline {section}{\tocsection {}{4}{The non--isentropic equations in symmetric hyperbolic form for $U=(p,u^\alpha ,S)$}}{3}{section.4}
\contentsline {subsection}{\tocsubsection {}{4.1}{Symmetric Hyperbolic Systems}}{3}{subsection.4.1}
\contentsline {subsection}{\tocsubsection {}{4.2}{Fluid decomposition}}{4}{subsection.4.2}
\contentsline {subsection}{\tocsubsection {}{4.3}{Modification of the fluid decomposed system}}{4}{subsection.4.3}
\contentsline {subsection}{\tocsubsection {}{4.4}{Symmetric hyperbolic form}}{5}{subsection.4.4}
\contentsline {section}{\tocsection {}{5}{Makino variable}}{7}{section.5}
\contentsline {section}{\tocsection {}{}{References}}{9}{section*.6}
