%%  -*-coding: utf-8 -*-
\providecommand{\main}{..}  % *Modification: redefine path location, must go before \documentclass
\documentclass[main.tex]{subfiles}


%% \ifusepkg
%% \usepackage{mypub}
%% \usepackage{hyperref}
%% \fi

\begin{document}

\makeatletter
\def\@rcsInfoFancyInfo{{\footnotesize%
    \emph{
\rcsInfoRevision,
%    {\color{blue}  \rcsInfoOwner,}
\fcolorbox{black}{yellow}{\rcsInfoOwner,}
      \rcsInfoDate,
  %    \rcsInfoTime}
  }}}
\makeatother


\rcsInfo $Id$ 

\section{The relativistic Euler equations with Entropy}
\label{sec:relativistic-case}
We now briefly introduce the notion of a relativistic perfect
isentropic fluid. 
See for example
\cite{christodoulou95:_self% Christodoulou 1995, Arch. Rational Mech. Anal. 130, 343
}
\cite{Choquet-Bruhat_09% Choquet-Bruhat 2009, xxvi+785, Oxford University Press, Oxford
}
  for more information.
For a perfect fluid, the energy-momentum tensor take the following
form
\begin{align}
  \label{E:EMTensorcDef}
  T^{\alpha \beta} = (\epsilon + p) u^{\alpha} u^{\beta}  + p g^{\alpha \beta} 
\end{align}
where $\epsilon$ is the {proper energy density} of the fluid, $p$ is the
{pressure}, and $u^{\alpha}$ is the {four-velocity}, which is subject to the
normalization constraint
\begin{align}
  \label{E:uNormalizedcSquared}
  g_{\alpha \beta} u^{\alpha} u^{\beta} = -1 % = -c^2.
\end{align}
The Euler equations for a perfect fluid are (see e.g. \cite{christodoulou95:_self% Christodoulou 1995, Arch. Rational Mech. Anal. 130, 343
})
\begin{align}
  \nabla_{\alpha} T^{\alpha \beta} &= 0 \qquad (\beta=0,1,2,3) \label{E:Euler} \\
  \nabla_{\alpha}(\varrho u^{\alpha}) &= 0,  \label{E:nandulaw}
\end{align}
where $\varrho$ is the \emph{proper number density} and $\nabla$ denotes the
covariant derivative induced by the spacetime metric $g.$ We also use
the \emph{Entropy}, a thermodynamic variable that we denote by $S$ and
which satisfies (see
\cite{pichon65:_etude% Pichon 1965, Ann. Inst. H. Poincaré Sect. A (N.S.) 2, 21
})
\begin{equation}
\label{eq:sec-02-non-isentropic:1}
  u^{\alpha}\nabla_{\alpha}(S) = 0,
\end{equation}

In order to close the system we have to introduce an equation of state, for
which we demand the following criteria.
\cite{guo99:_format% Guo \& Tahvildar-Zadeh 1999, 238, 151, in: Nonlinear partial differential equations ({E}vanston, {IL}, 1998), Amer. Math. Soc., Providence, RI
}):
\ubcomment{Conditions on the equation of state: Do we need that?}
\begin{enumerate}
  		
  \item $0\leq\epsilon $ is a function of $0\leq\varrho $ and $0\leq S.$

  \item $0\leq p$ is defined by
  \begin{align}
    p= \varrho \left.
    \frac{\partial \epsilon}{\partial \varrho} \right|_{S} -
    \epsilon,
    \label{E:pressure}
  \end{align}
%%   where the notation $\left. 
%%   \right|_{\cdot}$ indicates partial differentiation with $\cdot$ held
%%   constant.
  \item A perfect fluid satisfies
  \begin{align}
  0<  \left.
    \frac{\partial \epsilon}{\partial \varrho} \right|_{S}, 0<\left.
    \frac{\partial p}{\partial
    \varrho} \right|_{S}, 0\leq\left.
    \frac{\partial \epsilon}{\partial {S}} \right|_\varrho  \quad
    \mbox{with} \quad ``='' \quad \mbox{iff} \quad S=0.
    \label{E:EOSAssumptions}
  \end{align}
  As a consequence, we have that $\sigma,$ the speed of sound in the fluid,
  is always real for $0<S :$
  \begin{align}
    \sigma^2 \overset{def}{=} c^2 \left.\frac{\partial p}{\partial
    \epsilon}\right|_{S} = c^2 \frac{{\frac{\partial p}{ \partial \varrho}}|_{S}}{{\frac{\partial \epsilon}{ \partial    \varrho}}|_{S}} > 0.
    \label{E:SpeedofSoundc}
  \end{align}
  \item We also demand that the speed of sound is positive and less
  than the speed of light whenever $0<\varrho  $ and $0<S  $:
  \begin{align} \label{E:Causalityc}
   0< \varrho \ \mbox{and} \ 0< S   \implies 0 < \sigma < c.
  \end{align}
\end{enumerate}
    
Postulates $(1) - (3)$ express the laws of thermodynamics and
fundamental thermodynamic assumptions, while the last one ensures that
vectors that are timelike with respect to the sound cone are
necessarily timelike with respect to the light cone. 
%% that at each $x \in \mathcal{M},$ vectors that are causal with respect to the
%% sound cone in $T_x \mathcal{M}$ are necessarily causal with respect to
%% the gravitational null cone in $T_x \mathcal{M};$
    
\ubcomment{The different energy conditions: Do we need that}    
\begin{rem}[Energy condition]
  \label{rem:sec-02-non-isentropic:1}
  We note that the assumptions $\epsilon \geq 0, p \geq 0$ together imply that the
  energy--momentum  tensor \eqref{E:EMTensorcDef}
  satisfies
  \begin{enumerate}
    \item \label{item:sec-02-non-isentropic:1} the \emph{weak energy
      condition} ($T_{\alpha \beta} X^{\alpha} X^{\beta} \geq 0$ holds
    whenever $X$ is timelike and future-directed with respect to the
    gravitational null cone) and the
    \item \label{item:sec-02-non-isentropic:2} \emph{strong energy
      condition}
    ($[T_{\alpha \beta} - \frac{1}{2} g^{\alpha \beta}T_{\alpha \beta}
    g_{\alpha \beta}]X^{\alpha}X^{\beta} \geq 0$ holds whenever $X$ is
    timelike and future-directed with respect to the gravitational
    null cone). 
    \item \label{item:sec-02-non-isentropic:3} Furthermore, if we
    assume that the equation of state is such that $p=0$ when
    $\epsilon = 0,$ then \eqref{E:SpeedofSoundc} and \eqref{E:Causalityc}
    guarantee that $p \leq \epsilon$. 
    This implies the \emph{dominant energy condition}
    ($-T^{\alpha}_{\ \beta} X^{\beta}$ is causal and future-directed
    whenever $X$ is causal and future-directed with respect to the
    gravitational null cone).
  \end{enumerate}
\end{rem}
    
  
    
    
\section{An Example of an equation of state for the non--isentropic Euler equations}
\label{sec:equation-state-non}
We follow the convention of Choquet--Bruhat
\cite{Choquet_2009% Choquet--Bruhat 2009, Oxford Science Publications
})  denote a polytropic equation of state for an relativistic
non--isentropic fluid as follows. 
(See also
\cite{guo99:_format% Guo \& Tahvildar-Zadeh 1999, 238, 151, in: Nonlinear partial differential equations ({E}vanston, {IL}, 1998), Amer. Math. Soc., Providence, RI
}).
\begin{equation}
  \label{eq:sec-02-non-isentropic:3}
  \epsilon = \varrho + \frac{A(S)}{\gamma-1}\varrho^{\gamma}
\end{equation}
and by (\ref{E:pressure})
\begin{equation}
  \label{eq:sec-02-non-isentropic:2}
  p =  A(S)\varrho^{\gamma}.
\end{equation} 
\lkcomment{I changed the order because it  fits the definition by 
(\ref{E:pressure})}

An concrete example is:
\begin{equation}
\label{eq:section-0002-non-isentropic:3}
p = \frac{K}{3}\left( \frac{3S}{4K} \right)^{\frac{4}{3}}\varrho^{\frac{4}{3}}
\end{equation}

In the isentropic case ($S=const$), the corresponding
equation of state is denoted as the barotropic equation of state:
\cite{Choquet_2009% Choquet--Bruhat 2009, Oxford Science Publications
})
\begin{equation}
  \label{eq:non-isotropic-entropy-sh:98}
  p=K \varrho^{\gamma}
\end{equation}
\textbf{or}
\begin{equation}
  \label{eq:non-isotropic-entropy-sh:99}
  p=K \varepsilon^{\gamma}
\end{equation}
we will however use in your study  the equation of state of
type (\ref{eq:sec-02-non-isentropic:2}) and (\ref{eq:sec-02-non-isentropic:3}).


\biblio
\end{document}
