%%  -*-coding: utf-8 -*-
\providecommand{\main}{..}  % *Modification: redefine path location, must go before \documentclass\biblio
\documentclass[main.tex]{subfiles}

%% \ifusepkg
%% \usepackage{mypub}
%% \usepackage{hyperref}
%% \fi

\newcommand{\M}{{\mathord{\mathcal M}}}

\begin{document}

\section{Solutions of the constraints}
\label{sec:solut-constr}

In this section we will establish an existence theorem for the
solution to the constraint equations. 
Here the free data are $(h_{ab},A^{ab},y,v^a)$, where $h_{ab}$ is a
Riemannian metric, $A^{ab}$ is a divergence and trace free second order
form, $y$ represents the density and $v^a$ is the initial velocity
vector. 
We shall see that for the 
(\ref{eq:sec-02-non-isentropic:3})-(\ref{eq:sec-02-non-isentropic:2}) equation 
of state the
fluid data $(y,v^a)$ are independent from the geometric data
$(h_{ab},A^{ab})$, while for the barotropic equation of state
$p=K\epsilon^\gamma$, this is not the case.


We solve the constraints equations under the constant mean curvature condition.
There are methods to solve the constraint equations without CMC--condition 
\cite{Behzadan_Holst}, but for simplicity we shall refrain relying on them here.


Since we consider the Einstein--Euler systems on asymptotically flat
manifolds we start with the definition of this concept and the
functions spaces on these manifolds.
\ubcomment{Manifold or Manifolds, we have to make up our mind.}
\subsection{Asymptotically flat manifold}

A Riemannian manifold $(\mathcal{M},h)$ is asymptotically flat (or
asymptotically Euclidean) if the Riemannian metric $h$ tends to the
Eucildean metric at infinity. 
Often it is assumed that the manifold has finite numbers of ends. 
The  definition below is essentially due to Bartnik \cite{bartnik86}. 
Here  we consider only three dimensional manifolds.  

\begin{defn}
  Let $\mathcal{M}$ be a three dimensional smooth connected manifold and let
  $h$ be a metric on $\mathcal{M}$ such that $(\mathcal{M},h)$ is complete. 
  We say that $(\mathcal{M},h)$ is \textbf{asymptotically flat} of the class
  $H_{s,\delta}$, if $h_{ab}\in H_{s,\delta,{\rm loc}}$ and there is a compact set
  $\mathcal{K}\subset\mathcal{M}$ such that:
  \begin{enumerate}
    \item There is a finite collection of chards
    $\{U_i,\phi_i\}_{i=1}^N$ that covers
    $\mathcal{M}\setminus\mathcal{K}$, ($U_i$ is called an end), where
    $\phi_i$ is diffeomorpism between
    $E_i:=\{x\in \setR^3: |x|>r_i>0\}$ and $U_i\subset \mathcal{M}$.
    % \item For each $i$,
    % $\phi^{-1}(U_i)=E_i:=\{x\in \setR^3: |x|>r_i\}$ for some positive $r_i$.
    \item The pull--back $(\phi_i^\ast h)$ is uniformly equivalent to the
    Eucildean metric $e$ on $E_i$.
    \item For each $i$,
    $(\phi_i^\ast h)_{ab}-\delta_{ab}\in H_{s,\delta}(E_i)$, where
    $\delta_{ab}$ is the Kronecker symbol.
  \end{enumerate}
\end{defn}

We need now to define the $H_{s,\delta}$--norm on the asymptotically flat 
manifold. We start with the definition of the norm on an open set of $\setR^3$.
\lkcomment{This probably in the end will be in other section} 
Recall the the $H_{a,\delta} $-- norm is defined on the entire space, so if 
$\Omega\subset\setR^3 $ is an open set,
then
\begin{equation*} 
\|u\|_{H_{s,\delta}(\Omega)}=\inf_{f_{\mid_\Omega}=u}\|f\|_{H_{s,\delta}
(\setR^3) } .
\end{equation*}

The weighted Sobolev space on $\mathcal{M}$ is defined as follows. 
The charts $\{U_i,\phi_i\}_{i=1}^N$ cover the ends of $\mathcal{M}$, let
$\{V_j,\Theta_j\}_{j=1}^{N_0}$ be a collection of charts that cover
$\mathcal{K}$, where $\Theta_j$ is diffeomorpism between a ball in
$\setR^3$ and $V_j\subset\mathcal{M}$. 
Let $\{\chi_i,\alpha_j\}$ be a partition of unity subordinate to
$\{U_i,V_j\}$, then
\begin{equation}
 \label{eq:norm:1}
 \|u\|_{H_{s,\delta}(\mathcal{M})}=\sum_{i=1}^N\left\|\phi_i^\ast(\chi_i 
u)\right\|_{H_{s,\delta}(\setR^3)}+
\sum_{i=j}^{N_0}\left\|\Theta_j^\ast(\alpha_j 
u)\right\|_{H_{s,\delta}(\setR^3)}.
\end{equation} 
This norm depends on the partition of unity, but different partition
results in equivalent norms. 
It can be shown that the properties of the
$H_{s,\delta}(\setR^3)$--norm are valid also for the
$H_{s,\delta}(\mathcal{M})$--norm, for further details and proofs see
\cite[Appendix]{BK7}. 


\subsection{The Brill--Cantor criterion}

Let $(\mathcal{M},h)$ be an asymptotically flat manifold of class
$H_{s,\delta}$ and $R(h)$ be the scalar curvature. 
The manifold is said to be in the positive Yamabe class if
\begin{equation}
\label{eq:brill-cantor}
 \inf_{\varphi\in C_0^\infty(\mathcal{M})}\frac{\int_{\mathcal{M} 
}\left(|\nabla \varphi|^2_{h}+\frac{1}{8}R(g)\right)d\mu_h}{\|\varphi\|_{L^6}^2} 
> 0
\end{equation} 

The following theorem was proved by Maxwell 
\cite{maxwell06:_rough_einst% Maxwell 2006, J. Reine Angew. Math. 590, 1
}
\begin{thm}
\label{thm:brill-cantor}
  Let $(\mathcal{M},h)$ be an asymptotically flat manifold of class
  $H_{s,\delta}$ and $s>\frac{3}{2}$. 
  Then $(\mathcal{M},h)$ is positive Yamabe class if and only if there is a
  conformal metric $\bar{h}$ such that $R(\bar{h})=0$.
\end{thm}

The flat conformal metric is constructed by a positive  solution $\alpha $ to 
the elliptic equation \begin{equation*}
 \Delta_h\alpha+\frac{1}{8}R(h)\alpha=0, 
\end{equation*}
with $\alpha-1\in H_{s,\delta}(\mathcal{M}) $ and setting $\bar{h}=\alpha^4 h$. 
The existence of such solution is guarantees by the Brill--Cantor condition 
(\ref{eq:brill-cantor}). 

\subsection{The conformal method}
\lkcomment{
I describe it by steps, so the below is under work and does not need to  be 
edited.}
\ubcomment{Ok I will not touch it for the moment}


The free initial data are $(h_{ab}, K^{ab},y,v^a)$ and they are given on an 
asymptotically flat manifold $\M$,  where $h_{ab}$ is a Riemannian metric on 
$\M$, $K^{ab}$ 
is a trace--less second fundamental form, $y $ is a non--negative function and 
$v^a$ is a vector on $\M$. We assume that the second form satisfies the 
equation $\nabla_b K^{ab}=-8\pi y^{\frac{2}{\gamma-1}}v^a$  and $|v^a|_{h}<1$. 
We shall use the conformal methods to show that there is conformally equivalent 
Cauchy data $(\hat{h}_{ab}, \hat{K}^{ab},\hat{y},\hat{v}^a)$ that satisfy the 
constraint equations (\ref{eq:initial:1}) and (\ref{eq:initial:3}).



One of the advantages if the equation of state  
(\ref{eq:sec-02-non-isentropic:2}) and
(\ref{eq:sec-02-non-isentropic:3}) is that the velocity of sound, 
$\frac{\partial p}{\partial\epsilon}$, is  less 
than the light speed for all values of the pressure. This means that  the system 
(\ref{eq:compat:2}) is invertible  in the half strip $\{(y, v^a): y>0, 
|v^a|<1\}$ and the last  condition is invariant for scaling.  

We assume $(\M,h)$ is asymptotically flat of the class $H_{s,\delta}$, 
$K^{ab}\in H_{s-1,\delta+1}(\M)$ and $(y,v^a)\in H_{s,\delta}(\M)$. Let 
$\mathcal{S}_{s-1,\delta+1}(\M)$ a subspace of tensors in 
$H_{s-1,\delta+1}(\M)$ 
consists of all trace-less symmetric forms. Following Cantor \cite{} we shall 
first discuss  the splitting of $\mathcal{S}_{s-1,\delta+1}(\M)$ into two 
subspaces. 

Let
\begin{equation*} 
\mathbb{L}(W)=\mathcal{L}(W)-\frac
{2}{3}\left({\rm div}W\right)h
\end{equation*}
be the conformal  Killing field operator, where
$\mathcal{L}(W)_{ab}=\nabla_a W_b+\nabla_bW_a$  is the Lie derivative of the 
metric $h_{ab}$ with respect to the vector $W$. This  operator maps vectors in 
$H_{s,\delta}(\M)$ into $\mathcal{S}_{s-1,\delta+1}(\M)$. The Lichnerowicz 
Lpalacian is the composition of the  Killing field operator with the divergence
\begin{equation}
\label{eq:constraint:1}
 \Delta_{\mathbb{L}_{{h}}} W= \left({\rm div}\circ \mathbb{L}\right)(W)=
\Delta_{{h}} W+\frac{1}{3}{\nabla}\left({\rm div W}\right)+{\rm Ric}W,
\end{equation} 
and it is a linear elliptic operator
\begin{equation}
 \Delta_{\mathbb{L}_{{h}}}: H_{s,\delta}(\M)\to H_{s-2,\delta+2}(\M).
\end{equation}
This is a Fredholm operator on the 
weighted spaces with index zero for $-\frac{3}{2}<\delta<-\frac{1}{2}$ and its 
kernel consists of Killing vector fields. Maxwell 
\cite{maxwell06:_rough_einst}  showed that there are no non--trivial Killing 
vector fields in $H_{s,\delta}(\M)$, hence 
is an isomorphism. At that stage we apply standard theory of functional 
analysis (see e.g. \cite{Cantor}) and get
\begin{equation*}
 \mathcal{S}_{s-1,\delta+1}(\M)={\rm Im}(\mathbb{L})\oplus {\rm Ker}({\rm div}),
\end{equation*}
known as the York decomposition. 


Consequently, for any vector $j^a\in H_{s-2,\delta+2}(\M)$, the equation 
$\nabla_a K^{ab}=j^a$ has a solution in the form
\begin{equation}
 K^{ab}=A^{ab}+(K^\#)^{ab},
\end{equation} 
where ${\rm div} A=0$ and $K^\#=\mathbb{L}\left({\rm div}\circ 
\mathbb{L}\right)^{-1}(j)$.


In the present fluid data, $j^a=-8\pi y^{\frac{2}{\gamma-1}}v^a$, where 
$(y,v^a)\in H_{s,\delta}$, therefore the following lemma is necessary for the 
construction of the initial data.

\begin{lem}[Nonlinear estimate of power of functions]
  \label{prop:power}
  Suppose that $ y\in H_{s,\delta}(\M)$, $0\leq y$ and $\beta$ is a real
  number greater or equal $2$. 
  Then
  \begin{enumerate}
    \item If $\beta\in \setN $, $\frac{5}{2}<s$ and
    $\frac{2}{\beta-1}-\frac{3}{2}\leq \delta$, then
    \begin{equation}
      \label{eq:non:1}
      \|y^\beta\|_{H_{s-1,\delta+2}(\M)}\leq C_n
      \left(\|y\|_{H_{s,\delta}(\M)}\right)^\beta.
    \end{equation}
    \item If $\beta\not\in \setN$,
    $\frac{5}{2}<s<\beta-[\beta]+\frac{5}{2}$ and
    $\frac{2}{[\beta]-1}-\frac{3}{2}\leq \delta$, then
    \begin{equation}
      \label{eq:non:2}
      \|y^\beta\|_{H_{s-1,\delta+2}(\M)}\leq C_n
      \left(\| y\|_{H_{s,\delta}(\M)}\right)^{[\beta]}.
    \end{equation}

  \end{enumerate}

\end{lem}








% We solve the constraints equations under the constant mean curvature condition.
% There are methods to solve the constraint equations without CMC--condition 
% \cite{Behzadan_Holst}, but for simplicity we shall refrain relying on them here.


The given initial data consists of the geometric data $(h_{ab}, A^{ab})$, where 
$h_{ab}\in H_{s,\delta}$ and $A^{ab}\in H_{s-1,\delta}(\mathcal{M})$, and 
$h_{ab} $ satisfies the Brill-Cantor conditions (\ref{eq:brill-cantor}), and 
 $A^{ab}$ is  divergence--less and trace--less second form.
The 
fluid data $(y,v^a)\in H_{s,\delta}(\mathcal{M})$ with $|v^a|_{h}<1$. We assume 
that $s>\frac{3}{2}$ and $\delta>-\frac{3}{2}$ and we shall construct the 
initial data for the evolution equations step by step as follows:

\vskip 5mm
\noindent
 \textit{Step 1}. By theorem \ref {thm:brill-cantor} there is $\alpha >0$ on 
$\mathcal{M}$ with $\alpha-1\in H_{s,\delta}(\mathcal{M})$ and such that 
$\bar{h}_{ab}=\alpha^4 h_{ab}$  has zero scalar curvature. In order that the 
new velocity vector $\bar{v}^a$ will satisfy $|\bar{v}^a|_{\bar{h}}<1$ it 
scales $\bar{v^a}=\alpha^{-2}v^a$ and the new second fundamental form 
$\bar{A}^{ab}=\alpha^{-10}A^{ab}$ (see e.g. \cite[Chap. 18 \S9]{taylor97c}). 
Then also $\bar{j}^a=\alpha^{-10} j^a$ and  by (\ref{eq:compat:2}) we have 
$\bar{z}=\alpha^{-8} z$ and   $\bar{y}=\alpha^{-4(\gamma-1)}y$.

\vskip 5mm

\noindent
 \textit{Step 2}.  From the previous step it follows that we need to show that 
$\alpha^{-\beta}u\in H_{s,\delta}(\mathcal{M})$, when $u\in 
H_{s,\delta}(\mathcal{M}) $ and $\beta$ a positive constant. By writing 
$\alpha^{-\beta}u=(\alpha^{-\beta}-1)u+u$  it suffices to show 
that $(\alpha^{-\beta}-1)$ belongs to $H_{s,\delta}(\mathcal{M})$ and then to 
use the multiplication property (\ref{}).  However, we cannot apply Moser type 
estimate 
to $F(t)=t^{-\beta}-1$, since this function is not defined for $t=0$. In order 
to overcome this we note that since $(\alpha-1)\in 
H_{s,\delta}(\mathcal{M})$, therefore $\lim_{|x|\to \infty}\alpha(x) =1$ by the 
embedding  into the continuous (Proposition \ref{}). 
Hence the set $\{x\in \mathcal{M}: 
\alpha(x)\leq\frac{1}{2}\} $ is compact and implies that there is a positive 
constant $c_0$ such that $\alpha(x)\geq c_0$ for all $x\in\mathcal{M}$.
Let now $H(t)$ be a $C^\infty$ function such that $H(t)=0$ for 
$t\leq \frac{c_0}{3}$, $H(t)=1$ for $\frac{2c_0}{3}\leq t$ and set 
$G(t)=H(t)F(t)$. Then $G$ is a smooth function, $G(0)=0$, and therefore by 
Proposition \ref{}, $\|G(u)\|_{H_{s,\delta}}\leq C \|u\|_{H_{s,\delta}}$ for 
$s>\frac{3}{2}$ and $\delta>-\frac{3}{2}$. Since $G(\alpha)=F(\alpha)$, we have 
that $\alpha^{-\beta}-1\in H_{s,\delta}$. Consequently $\bar{A}^{ab}\in 
H_{s-1,\delta+1}(\mathcal{M})$,  $(\bar{y},\bar{v}^a)\in 
H_{s,\delta}(\mathcal{M})$ and similarly   $\bar{h}_{ab}\in 
H_{s,\delta}(\mathcal{M})$.

\vskip 5mm

\noindent\textit{Step 3}.
Consider the Killing field operator
\begin{equation*} 
\left(\bar{\mathcal{L}}(W)\right)_{ab}=\bar{\nabla}_aW_b+\bar{\nabla}_bW_a-\frac
{2}{3}\bar{h}_{ab}\left(\bar{\nabla}_cW^c\right)
\end{equation*}
 and  the Lichnerowicz--Laplacian
\begin{equation}
\label{eq:constraint:1}
 \left(\Delta_{L_{\bar{h}}} W\right)^b=\bar{\nabla}_a\circ\left( 
\bar{\mathcal{L}}(W)\right)^{ab}= 
 \Delta_{\bar{h}} 
W^b+\frac{1}{3}\bar{\nabla}^b\left(\bar{\nabla}_aW^a\right)+\bar{R}_a^bW^a,
\end{equation} 
 both  with respect to the metric $\bar{h}$ and where $\bar{R}_a^b$ is the 
Ricci curvature tensor.

Maxwell \cite[Theorem 4.6]{maxwell06:_rough_einst} showed that 
\begin{equation*}
 \Delta_{L_{\bar{h}}}: H_{s,\delta}(\M)\to H_{s-2,\delta+2}(\M)
\end{equation*}
is isomorphism. Therefore, if $\bar{j}^b$ belongs to $ H_{s-2,\delta+2}(\M)$, 
then equation $\left(\Delta_{L_{\bar{h}}}W\right)^b=\bar{j}^b$ has a unique 
solution $W$. Hence, letting    
\begin{equation*}
 \bar{K}^{ab}=\bar{A}^{ab}+\left(\bar{\mathcal{L}}(W)\right)^{ab},
\end{equation*}
we have that $\bar{h}_{ab}\bar{K}^{ab}=0$ and 
$\bar{\nabla}_a\bar{K}^{ab}=\hat{j}^b$, and consequently $\bar{K}^{ab}$ 
satisfies the momentum constraint 
(\ref{eq:initial:3}).


Thus it remain to 
show that $\bar{j}^a\in H_{s-2,\delta+2}(\mathcal{M})$.
Recalling that  $\bar{j}^b=\bar{z}\bar{v}^b$ and 
$\bar{z}=\bar{y}^{\frac{2}{\gamma-1}}$, 
so by \cite[Proposition 12]{Euler-Poisson}, $\bar{z}\in 
H_{s,\delta+2}(\mathcal{M})\subset H_{s-2,\delta+2}(\mathcal{M})$ for 
$\gamma\leq \frac{3}{2}$ and when 
$\frac{5}{2}<s<\frac{2}{\gamma-1}+\frac{3}{2}$.
\lkcomment{Here we need $s>\frac{5}{2}$, though that for the elliptic 
equation we can do it with $s>\frac{3}{2}$. Anyhow, for the evolutions 
equations we need $s>\frac{5}{2}$.}
Using again the algebra property (\ref{}), we have that $\bar{j}^a\in 
H_{s-2,\delta+2}$.
So we conclude that $\bar{K}^{ab}$ is a traceless tensor that satisfies 
the constraint equation (\ref{eq:constraint:3}).

\vskip 5mm

\noindent\textit{Step 4}.
In order to solve the Hamiltonian constraint (\ref{eq:initial:1}) we need an 
additional scaling. Setting $\hat{h}_{ab}=\phi^4\bar{h}_{ab}$, and scaling the 
other quantities appropriately: $\hat{K}^{ab}=\phi^{-10}\bar{K}^{ab}$ and 
$\hat{z}=\phi^{-8}\bar{z}$. Then 
\begin{equation*}
-8\Delta_{\bar{h}}\phi+R(\bar{h})\phi=R(\hat{h})\phi^5
\end{equation*}
(see e.g. \cite[\S 5.1]{Aubin_1998}), and since $R(\bar{h})=0$ and 
$\bar{K}^{ab}$ is traceless, the  Hamiltonian constraint (\ref{eq:initial:1}) 
is equivalent to the non-linear  Lichnerowicz equation
\begin{equation}
-\Delta_{\bar{h}}\phi=\left(2\pi\bar{z}\right)\phi^{-3}+\left(\frac{1}{8}\bar{K}
_a^b\bar { K }_b^a\right)\phi^{-7}. 
\end{equation} 
We set now $u=\phi -1$, then 
\begin{equation}
-\Delta_{\bar{h}}u=\left(2\pi\bar{z}\right)(u+1)^{-3}+\left(\frac{1}{8}\bar{
K }_a^b\bar { K }_b^a\right)(u+1)^{-7},
\end{equation} 
and since $\bar{z}\in H_{s-2,\delta+2}(\M)$ (obviously also $\bar{
K }_a^b\bar { K }_b^a$ in that space), we can apply \cite[Theorem 3.8]{BK3} 
(\cite[Theorem 4.1]{BK7}) to obtain a unique non-negative solution $u\in 
H_{s,\delta}(\M)$. Therefore $\phi\geq 1$ and $\phi-1\in H_{s,\delta}(\M)$.  
So by the multiplication property (\ref{}), $(\M,\hat{h})$ is asymptotically 
flat in the class $H_{s,\delta}$ and  $\hat{K}^{ab}$ is also in 
$H_{s-1,\delta+1}(\M)$.
\lkcomment{I do not see a reference to that that theorem in Maxwell  
\cite{maxwell06:_rough_einst}}

\vskip 5mm
\noindent\textit{Step 5.} We need now to construct the initial data for the 
fluid. We set $\hat{y}=\phi^{-4(\gamma-1)}\bar{y}$ and 
$\hat{v}^a=\phi^{-2}\bar{v}^a$, and since $\phi\geq 1$ and $\phi-1\in 
H_{s,\delta}(\M)$, then $(\hat{y},\hat{v}^{a})\in H_{s,\delta}$. furthermore,
$\hat{h}_{ab}\hat{v}^a\hat{v}^b=\bar{h}_{ab}\bar{v}^a\bar{v}^b={h}_{ab}{v}^a
{v}^b<1$. Hence at that stage we can apply Proposition \ref{prop:constraction} 
and let $(w,u^a)=\Phi^{-1}(\hat{y},\hat{v}^a)$, then under the equation of 
state (\ref{eq:sec-02-non-isentropic:2}) and (\ref{eq:sec-02-non-isentropic:3}), 
the compatibility condition (\ref{eq:initial:4}) holds with $(\hat{z},\hat{j}^a) 
$ as the left had side.  By Moser type estimate $(w,u^a)\in H_{s,\delta}(\M)$.
Thus, $(w,u^0=1-\hat{h}_{ab}u^au^b, u^a)\in H_{s,\delta}(\M)$ are the initial 
data for of the fluid.




\begin{thm}[Construction of initial data] 
Given freely initial data $(h_{ab},A^{ab},y,v^a)$ on asymptotically falt 
manifold $\M$ of the class $H_{s,\delta}$, where $A^{ab}\in 
H_{s-1,\delta+1}(\M)$ divergence--less and trace--less form,   
 $(y,v^a)\in H_{s,\delta}(\M)$ such that $y\geq 0$ and 
$h_{ab}v^av^b<1$,  and $\frac{5}{2}<s<\frac{2}{\gamma-1}+\frac{3}{2}$, 
$-\frac{3}{2}+\frac{2}{\left[\frac{2}{\gamma-1}\right]-1}<\delta<-\frac{1}{2}$ 
and $1<\gamma\leq\frac{3}{2}$. Then for  the equation of 
state (\ref{eq:sec-02-non-isentropic:2}) and 
(\ref{eq:sec-02-non-isentropic:3}) the data  $(h_{ab},A^{ab},y,v^a)$ is 
conformally equivalent to  $(\hat{h}_{ab},\hat{K}^{ab},\hat{y},\hat{v}^a)$, they 
satisfies  the constraint equations (\ref{eq:initial:1})-(\ref{eq:initial:3}) 
and the compatibility condition (\ref{eq:initial:4}) with 
$\hat{z}=\hat{y}^{\frac{2}{\gamma-1}}$ and $\hat{j}^a=\hat{z}\hat{v}^a$.  

\end{thm}

\begin{rem}
Unlike \cite[Theorem 2.6]{BK3}, the initial data $y$ and $v^a$ are given freely 
here. That is one of the advantages if the equation of state  
(\ref{eq:sec-02-non-isentropic:2}) and
(\ref{eq:sec-02-non-isentropic:3}), for which the velocity of sound is less 
than the light speed for all values of the pressure.  
\end{rem}






\biblio
\end{document}
