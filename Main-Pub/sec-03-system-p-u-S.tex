%%  -*-coding: utf-8 -*-
\providecommand{\main}{..}  % *Modification: redefine path location, must go before \documentclass\biblio
\documentclass[main.tex]{subfiles}
%% \ifusepkg
%% \usepackage{mypub}
%% \usepackage{hyperref}
%% \fi

\begin{document}



\section{The system $U=(p,u^\alpha,S)$}
\label{sec:system-u=p-ualpha}

The non relativistic non isentropic Euler equations can be written in
symmetric hyperbolic form, provided the variables are
$U=(p,u^\alpha,S)$. 
We will follow this approach in this sections, because of two reasons.
Firstly it will enable use to write the system in symmetric hyperbolic
form also for the relativistic non isentropic equations, a fact we
have not seen so far in the literature and which we will discuss to
some extend in the appendix. 
Secondly the system for $U=(p,u^\alpha,S)$ is a convenient starting
point to introduce the regularizing Makino variable.

\subsection{Fluid decomposition}
\label{sec:fluid-decomposition}


First we apply the well known fluid decomposition (see for example 
\cite{BK8% Brauer \& Karp 2014, Comm. Math. Physics 325, 105
}) to equation
(\ref{E:Euler}), which results in.
\begin{align}
  \label{eq:non-isotropic-entropy-sh:25}
  u_\beta\nabla_{\nu}T^{\nu\beta} &=  0\\
  P_{\alpha\beta}\nabla_{\nu}T^{\nu\beta} &= 0
\end{align}
and from which we obtain
\begin{align}
  \label{eq:euler-rel:10}
  u^{\nu}\nabla_{\nu}\epsilon + (\epsilon+p) \nabla_{\nu}u^{\nu}    &= 0 \\
  \label{eq:non-isotropic-entropy-sh:26}
  (\epsilon+p)   P_{\alpha\beta}u^{\nu}\nabla_{\nu}{u^\beta} + P^\nu{}_\alpha    \nabla_\nu p     &=    0.
\end{align}
Note that equation (\ref{E:pressure}) implies
\begin{equation}
\label{eq:sec-03-system-p-u-S:1}
p+\epsilon = \varrho \frac{\partial\epsilon}{\partial\varrho} \qquad\mbox{or}\qquad \left(  p+\epsilon  \right) \frac{\partial\varrho}{\partial\epsilon}= \varrho 
\end{equation}
and therefore equation (\ref{eq:euler-rel:10}) is equivalent (as long
it is a $C^1$ solution) to
\begin{equation}
\label{eq:sec-03-system-p-u-S:2}
  u^{\nu}\nabla_{\nu}\varrho + \varrho \nabla_{\nu}u^{\nu}    = 0 
\end{equation}
which we will use from now on. 
As usual, in order to obtain a symmetric hyperbolic system we have to
modify it in the following way. 
The normalisation condition (\ref{E:uNormalizedcSquared})
results in
\begin{equation}
  \label{eq:rel-euler-symm:7}
  u_\beta u^\nu\nabla_\nu u^\beta= 0
\end{equation}
so we add
\begin{equation}
  \label{eq:rel-euler-symm:8}
\varrho u_\beta u^\nu \nabla_\nu u^\beta = 0
\end{equation}
to equation (\ref{eq:sec-03-system-p-u-S:2}) and
\begin{equation}
  \label{eq:rel-euler-symm:9}
  u_\alpha u_\beta u^\nu\nabla_\nu u^\beta= 0
\end{equation}
to (\ref{eq:non-isotropic-entropy-sh:26}) and obtain finally 
\begin{align}
  \label{eq:eineul:8}
  u^{\nu}\nabla_{\nu}\varrho + \varrho P^\nu{}_\beta\nabla_\nu u^\beta&= 0 \\
  \label{eq:publ-broken:3}
  (\epsilon+p)  \Gamma_{\alpha\beta}    u^{\nu}\nabla_{\nu}u^{\beta} +P^\nu{}_\alpha
  \nabla_\nu p  &= 0,
\end{align}
which together with equation (\ref{eq:sec-02-non-isentropic:1})
form our system of equations.

Here
\begin{equation}
  \label{eq:rel-euler-symm:10}
  \Gamma_{\alpha\beta}= P_{\alpha\beta}+u_{\alpha}u_{\beta}= g_{\alpha\beta}+2u_{\alpha}u_{\beta}
\end{equation}


We now use the equation of state (\ref{eq:sec-02-non-isentropic:2})
and (\ref{eq:sec-02-non-isentropic:3}) but it could be any equation of
state of the form $p=f(\varrho,S$), which implies
\begin{equation}
\label{eq:sec-03-system-p-u-S:3}
 \nabla_{\nu} p= \frac{\partial p}{\partial\varrho}  \nabla_{\nu}\varrho +
 \frac{\partial p}{\partial S}  \nabla_{\nu}S
\end{equation}
from which we can conclude using  equation
(\ref{eq:sec-02-non-isentropic:1})
that
\begin{equation}
\label{eq:sec-03-system-p-u-S:4}
u^{\nu} \nabla_{\nu} p= \frac{\partial p}{\partial\varrho}u^{\nu}  \nabla_{\nu}\varrho 
\end{equation}
so that we finally obtain the system.
\begin{align}
\label{eq:non-isotropic-entropy-sh:113}
  u^{\nu}\nabla_{\nu} p + \varrho\frac{\partial p}{\partial \varrho} P^\nu{}_\beta\nabla_\nu u^\beta&= 0 \\
\label{eq:non-isotropic-entropy-sh:114}
  (\epsilon + p)  \Gamma_{\alpha\beta}    u^{\nu}\nabla_{\nu}u^{\beta} +P^\nu{}_\alpha  \nabla_\nu p  &= 0\\
\label{eq:non-isotropic-entropy-sh:115}
  u^{\alpha}\nabla_{\alpha}S&=0
\end{align}

\begin{rem}
  If we use the relation (\ref{eq:sec-03-system-p-u-S:1}) we can
  transform our system
  (\ref{eq:non-isotropic-entropy-sh:113})--(\ref{eq:non-isotropic-entropy-sh:115})
  into the form provided by Guo-Tahvildar-Zadeh, namely
  \begin{align}
  \label{eq:non-isotropic-entropy-sh:72}
  \displaystyle\frac{1}{(\epsilon+p)\sigma}u^\nu\partial_\nu p + \sigma \partial_\nu
  u^\nu
  & =  0 \\
  \label{eq:non-isotropic-entropy-sh:73}
  \sigma P^{\mu\nu}\partial_\nu p + (\epsilon+p)\sigma u^\nu \partial_\nu u^\mu  & =  0\\
  \label{eq:non-isotropic-entropy-sh:74}
  u^\nu \partial_\nu S & = 0.
\end{align}

\end{rem}

\end{document}
